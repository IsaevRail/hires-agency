'use strict';

var app = {

  init: function () {

    app.header.toggle();
    app.header.scrollFixed();
    app.search.selected();
    app.slider.init();
    app.product.init();
    app.sidebar.init();
    app.dialog.init();

    $(window).on('resize', function (e) {

      var window = $(this);

      app.fullscreenVideo.w_h(window);
    }).trigger('resize');
  },

  header: {

    toggle: function () {

      $('#toggle-menu').on('click', function (e) {
        e.preventDefault();

        var $this = $(this);
        var header_right = $('.header__right');

        if ($this.hasClass('-active')) {
          $this.removeClass('-active');
          header_right.removeClass('-active');
        } else {
          $this.addClass('-active');
          header_right.addClass('-active');
        }

        $(document).on('click', function (e) {

          if (!$(e.target).closest('.header__right').length) {
            $this.removeClass('-active');
            $('.header__right').removeClass('-active');
          }
        });
      });

      $(window).on('scroll', function () {
        $('#toggle-menu').removeClass('-active');
        $('.header__right').removeClass('-active');
      });

      $('.lang__link').on('click', function (e) {
        e.preventDefault();

        var $this = $(this),
            $container = $this.closest('.lang'),
            $item = $container.find('.lang__link');

        $item.removeClass('-active');
        $this.addClass('-active');
      });
    },

    scrollFixed: function () {

      if (!$('body').hasClass('home-page')) return;

      var header = $('.header');

      $(window).on('scroll', function (e) {

        var scroll = $(window).scrollTop();

        if (scroll >= 400) {

          header.addClass('-hide');

          setTimeout(function () {
            header.addClass('-fixed');
          }, 10);
        } else {
          header.removeClass('-fixed');
          header.removeClass('-hide');
        }
      });
    }

  },

  fullscreenVideo: {

    w_h: function (window) {

      var window_w = window.width();
      var window_h = window.height();
      var window_h_2 = window_h * 1.775;

      var fullscreen = $('.mainscreen__fullscreen');

      if (window_w > window_h_2) {
        fullscreen.removeClass('-height');
      } else {
        fullscreen.addClass('-height');
      }
    }

  },

  slider: {

    init: function () {

      $('#slider-video').slick({
        dots: true,
        responsive: [{
          breakpoint: 768,
          settings: {
            arrows: false
          }
        }]
      });

      $('.viewed__slider').slick({
        slidesToShow: 4,
        responsive: [{
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            arrows: false,
            dots: true
          }
        }]
      });
    }

  },

  search: {

    selected: function () {

      $('select').selectric();
    }

  },

  product: {

    init: function () {

      var product = $('.product__info'),
          input = product.find('.checkbox__input'),
          price = product.find('.product__add_price');

      input.on('change', function () {

        var $this = $(this);

        if ($this.prop('checked')) {

          var $price = $this.closest('.product__quality_item').find('.product__quality_price').text();

          price.text($price);
        }
      });
    }

  },

  sidebar: {

    init: function () {

      var sidebar = $('.sidebar');
      var sidebar_open = $('.sidebar__open');
      var sidebar_close = $('.sidebar__close');

      sidebar_open.on('click', function (e) {
        e.preventDefault();

        sidebar.addClass('-active');
      });

      sidebar_close.on('click', function (e) {
        e.preventDefault();

        sidebar.removeClass('-active');
      });
    }

  },

  dialog: {

    init: function () {

      $('.dialog__form_editable').on('keypress keydown input', function (e) {

        if ($(this).text().length > 0) {
          $('.dialog__form_placeholder').hide();
        } else {
          $('.dialog__form_placeholder').show();
        }
      });
    }

  }

};

$(document).ready(app.init());